from django.apps import AppConfig

class ImportMembersConfig(AppConfig):
    name = 'import_members'
    label = name
    verbose_name = 'Import members from old MemberDB'
