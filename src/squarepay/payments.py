"""
This file contains functions for dealing with payments (although that's fairly obvious)
"""
import logging

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from square.client import Client

log = logging.getLogger('squarepay')

# load the configuration values
app_id = getattr(settings, 'SQUARE_APP_ID', None)
loc_id = getattr(settings, 'SQUARE_LOCATION', None)
access_key = getattr(settings, 'SQUARE_ACCESS_TOKEN', None)

# make sure the configuration values exist
if (app_id is None) or (loc_id is None) or (access_key is None):
    raise ImproperlyConfigured("Please define SQUARE_APP_ID, SQUARE_LOCATION and SQUARE_ACCESS_TOKEN in settings.py")

client = Client(access_token=access_key, environment='production')

def try_capture_payment(card_payment, source_id, verification_token):
    """
    attempt to charge the customer associated with the given card nonce (created by the PaymentForm in JS)
    Note: this can be called multiple times with the same CardPayment instance but the customer will not
    be charged multiple times (using the Square idempotency key feature)
    Returns either True on success or False on failure.
    """
    request_body = {
        'idempotency_key': card_payment.idempotency_key,
        'source_id': source_id,
        'verification_token': verification_token,
        'amount_money': {
            'amount': card_payment.amount,
            'currency': 'AUD'
        }
    }

    result = client.payments.create_payment(body=request_body)

    if result.is_success():
        card_payment.set_paid()
        return {
            "success": True,
            "receipt_url": result.body["payment"]["receipt_url"],
        }
        # Call the error method to see if the call failed
    elif result.is_error():
        return {
            "success": False,
            "errors": result.errors,
        }

