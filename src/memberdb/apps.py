from django.apps import AppConfig

class MemberConfig(AppConfig):
    name = 'memberdb'
    label = name
    verbose_name = "UCC Member Database"